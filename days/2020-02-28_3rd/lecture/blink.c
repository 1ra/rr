// blink

#include <avr/io.h>
#include <util/delay.h>

int main()
{
	DDRB = 0xFF; // Настраиваем порт ввода-вывода B на ВЫВОД
	PORTB = 0xFF; // Устанавливаем все ноги порта B на высокий уровень

	while (1) { //
		_delay_ms(100); // Задерживаем исполнение кода на 100 мс
		PORTB = ~PORTB; // Инвертируем сигнал на порте B
	}

	return 0;
}