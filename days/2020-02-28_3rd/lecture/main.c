#include <stdio.h>
#include <stdint.h>

void foo(int *ptr)
{
	*ptr = 42;
}

int main(void)
{
	int a = 1, b = 2, c = 3, d[] = { 4, 5, 6 }, *p;
	uint64_t pig = 0xDEADBEEF11111111;

	p = d;

	/* Массивы и указатели на их первые элементы - почти одно и то же */

	for (int i = 0; i < 3; ++i)
		printf("%d, ", d[i]); // Prints "4, 5, 6, "

	printf("\n");

	for (int i = 0; i < 3; ++i)
		printf("%d, ", p[i]); // Also prints "4, 5, 6, "

	printf("\n");

	for (p = d; p < (d + 3); ++p)
		printf("%d, ", *p); // Prints the same

	printf("\n");

	/* Указатели позволяют менять переменные,
       даже если они вне области видимости */

	p = &c;

	foo(p);

	printf("c = %d\n",
	       c); // Выведет 42, несмотря на то, что с не видна внутри foo()

	/* Типы приводятся явно и неявно */

	printf("%d divide by %d equals %f \n", a, b, a / b);
	printf("%d divide by %d equals %f \n", a, b, (float)(a / b));
	printf("%d divide by %d equals %f \n", a, b, (float)a / b);
	printf("%d divide by %d equals %d \n", a, b, (float)a / b);

	/* Указатели тоже иногда можно приводить к разным типам */

	p = (int *)&pig;

	for (int i = 0; i < 2; ++i)
		printf("%x \n", p[i]);

	return 0;
}