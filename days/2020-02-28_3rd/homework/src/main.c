#include <stddef.h>
#include <stdio.h>

#include "hw3.h"

/**
 * Инвертирует цвет у монохромного BMP файла.
 * 
 * Выполняет (не копируя данные) инверсию цвета в изображении,
 * сохранённом в формате BMP.
 * 
 * @param file Массив байтов, соответствующих
 *             спецификации файла BMP.
 * @param size Длина массива байтов входного файла.
 */
void invert(unsigned char *file, size_t size)
{
	// Ваша реализация:
}

int main(void)
{
	invert(homework_pic, sizeof homework_pic);

	if (!save_pic("result.bmp"))
		printf("Sorry, canot save the picture.\r\n");
	else
		printf("Successfully saved the picture.\r\n"
		       "The picture shoud be on the WHITE background now.\r\n");

	return 0;
}