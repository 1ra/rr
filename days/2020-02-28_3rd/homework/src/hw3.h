#ifndef HW3_H
#define HW3_H

#include <stddef.h>

#define PIC_LEN 32898

typedef unsigned char picture[PIC_LEN];

picture homework_pic;

int save_pic(const char *name);

#endif