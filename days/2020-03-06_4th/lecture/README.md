# Занятие 4

## Просто программирование

Разобрали две сущности: ключевое слово `static`, [позволяющее](https://www.geeksforgeeks.org/static-variables-in-c/) сохранять состояние функции
между вызовами[^1], и некоторые побитовые операции.

[^1]: Приведённая статья на английском. Помните, что английский нужно учить, но в крайнем случае [есть](https://chrome.google.com/webstore/detail/google-translate/aapbdbdomjkkjkaonfhkkikfgjllcleb?hl=ru) google [translate](https://addons.mozilla.org/ru/firefox/addon/to-google-translate/)

### Побитовые операции

Весьма полезный при работе на низком уровне аппарат. Большинство побитовых операций совершается "мгновенно" (за один такт),
что делает их эффективными, а появляющаяся благодаря ним возможность доступа к произвольному биту позволяет
экономить место, помещая, например, до восьми булевых (истина/ложь) значений в один байт.

Почитать можно [тут](https://learnc.info/c/bitwise_operators.html) или [тут](http://neerc.ifmo.ru/wiki/index.php?title=Побитовые_операции) - довольно подробно.

Выкладываю код, который был на паре - см. файл `bitwise_1.c` в этой папке. Он претендует на то, чтобы самому себя объяснять[^2].

[^2]: Напоминаю, что файл прокомментирован в стиле [Doxygen](http://www.doxygen.nl/), который позволяет автоматически генерировать документацию на основе исходных кодов вашей программы

## Программирование микроконтроллеров

Ничего не успели. Оставлю здесь пачку документации, чтобы потом на неё ссылаться. Можете
осмотреться, попривыкать, посмотреть, подобавлять в закладки и т.д.

+ [Страничка](https://www.microchip.com/wwwproducts/en/ATmega328p) микроконтроллера ATMega328p на сайте производителя
+ [Даташит](http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega48A-PA-88A-PA-168A-PA-328-P-DS-DS40002061A.pdf) его (и не только)
+ [Цоколёвка](#цоколёвка-pro-mini) Arduino Pro Mini
+ [Мануал](http://savannah.nongnu.org/projects/avr-libc/) по библиотеке для работы с контроллерами AVR в языке C
+ [Гайд](http://www.microchip.com//wwwAppNotes/AppNotes.aspx?appnote=en590906) по эффективному программированию на C (пригодится в будущем, если будете работать с МК)


### Цоколёвка Pro Mini

![Arduino Pro Mini](https://matrix.phys-el.ru/_matrix/media/r0/download/matrix.phys-el.ru/YZqVrVvzPffAuFeXxCYVcnsT)
