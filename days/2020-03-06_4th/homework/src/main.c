/**
 * @file main.c
 * 
 * Домашнее задание к четвёртому занятию (в освновном на битовые операции)
 */

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hw4.h"

/**
 * Конвертирует номер кодовой точки юникода в последовательность байтов.
 * 
 * Определяет, сколько байтов требуется для кодирования символа в кодировке UTF-8,
 * формирует эти байты и записывает в переменную `sym`, дополняя её нулями до
 * четырёх байтов, если для кодировки требуется меньше.
 * 
 * @param cpoint Номер кодовой точки
 * @param num Указатель на переменную, в которую будет записано количество байт
 * @param sym Массив однобайтовых символов длиной <= 4, в который будет записан UTF-8
 */
void codepoint2utf8(long cpoint, int *num, char *sym)
{
	// Ваш код здесь
}

int main()
{
	int i;
	char code_point[10]; // тут можно хранить "число", пока оно ещё строка
	char out_buff[strlen(homework_txt)];
	int num =
		0; // сюда можно записать количство байтов в текущем юникод-символе
	char tmp[4]; // тут можно временно хранить utf-8, который получился

	for (i = 0; i < strlen(homework_txt); ++i) {
		// Ваш код здесь (ниже дан пример, который копирует текст без изменений)
		out_buff[i] = homework_txt[i];
	}

	out_buff[i] = '\0'; // Нуль-терминируем, чтобы сработала strlen()

	write_message("msg.txt", out_buff, strlen(out_buff));
}
