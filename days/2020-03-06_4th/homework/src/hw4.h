#ifndef HW4_H
#define HW4_H

#include <stddef.h>

extern char homework_txt[];

int write_message(char *name, char *buff, size_t len);

#endif
