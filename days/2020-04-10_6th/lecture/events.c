/**
 * @file events.c
 *
 * Пример работы с кнопкой (обработка единичных нажатий)
 *
 * Программирует контроллер так, чтобы по одинарному нажатию
 * на кнопку менялся режим работы "гирлянды" (мигающего светодиода)
 */

#include <avr/io.h>
#include <util/delay.h>

/**
 * Режимы работы "гирлянды": быстрое мигание, медленное мигание
 */
#define MODE_FAST 0
#define MODE_SLOW 1

/**
 * Настройки длины интервалов, в которые светодиод горит и не горит,
 * в каждом из двух режимов
 */
#define SLOW_PAUSE_LEN 500
#define SLOW_PULSE_LEN 500
#define FAST_PAUSE_LEN 100
#define FAST_PULSE_LEN 100

uint8_t mode = MODE_FAST; // начальный режим
uint16_t pause_len = FAST_PAUSE_LEN, pulse_len = FAST_PULSE_LEN;
uint8_t prev_state; // не изициализируем - измерим реальное при старте

/**
 * Меняет режим "гирлянды".
 */
void change_mode(void)
{
	if (MODE_FAST == mode) {
		mode = MODE_SLOW;
		pause_len = SLOW_PAUSE_LEN;
		pulse_len = SLOW_PULSE_LEN;
	} else {
		mode = MODE_FAST;
		pause_len = FAST_PAUSE_LEN;
		pulse_len = FAST_PULSE_LEN;
	}
}

/**
 * Совершает акт единичного мигания (1 период)
 */
void do_blink(void)
{
	// так как встроенные _delay_*s требуют константы
	// в качестве аргумента, мы вынуждены
	// сделать цикл вместо _delay_ms(pause_len)
	for (uint16_t i = 0; i < pause_len; ++i)
		_delay_ms(1);

	PORTB |= _BV(PORTB5); // включаем светодиод

	for (uint16_t i = 0; i < pulse_len; ++i)
		_delay_ms(1);

	PORTB &= ~_BV(PORTB5); // выключаем светодиод
}

/**
 * Проверяет, нажали ли кнопку между текущим моментом и прошлой проверкой.
 */
uint8_t was_pressed(void)
{
	uint8_t current_state = !(PINB & _BV(PINB4));
	uint8_t x = (current_state && !prev_state);

	prev_state = current_state;

	return x; // 1 если кнопка нажалась
}

int main()
{
	DDRB  = _BV(PORTB5);
	prev_state = !(PINB & _BV(PINB4));

	while (1) {
		if (was_pressed())
			change_mode();

		do_blink();
	}
}

