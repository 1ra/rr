from subprocess import PIPE, Popen, call

Import("env", "projenv")


def foo(source, target, env):
    print('Disassembling the .elf file')
    with open('{}.s'.format(projenv['PROGNAME']), 'w') as f:
        call(["avr-objdump", '-d', '{}'.format(target[0])], stdout=f)


env.AddPostAction("$BUILD_DIR/${PROGNAME}.elf", foo)
