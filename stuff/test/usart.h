#ifndef USART_H
#define USART_H

#include <stdint.h>

#define USART_BAUD 250000
#define USART_TIMEOUT 50
#define USART_BUFF_LEN 256
#define USART_BUFF_MASK 0x1F // for len = 32

#define USART_SUCCESS 0
#define USART_ERR_READFULL -1
#define USART_ERR_WRITEFULL -2
#define USART_ERR_MAXATTEMPTS -3
#ifdef MCU_VERIFIES
#define USART_ERR_SIGN -4
#endif

#define RINC(X) (++X)
/*++X; \
        X &= USARTBUFF_MASK;*/
#define RDEC(X) (--X)
/*--X; \
        X &= USARTBUFF_MASK;*/

#define SKIP_CHAR() (null = UDR0)
#define USART_SEND_MAXATTS 800000
#define USART_MAX_ATTEMPTS 100000
#define CRC8_SEED 0x00

#ifdef MCU_VERIFIES
#define USART_READ(DATA) (usart_read_safe(DATA))
#else
#define USART_READ(DATA) (usart_read_blocking(DATA))
#endif

// Helper stuff

void usart_init(void);
void usart_rx_cli(void);
void usart_rx_sei(void);

// Sending routines

int8_t usart_enqueue(uint8_t data);
int8_t usart_send(void);
int8_t usart_write_blocking(uint8_t data);
#ifdef MCU_SIGNS
int usart_enqueue_crc();
#endif
int8_t usart_flush_tx(void);

// Readinf routines

uint8_t usart_read_unsafe(void);
uint8_t usart_available(void);
int8_t usart_read_blocking(volatile uint8_t *data);
#ifdef MCU_VERIFIES
int usart_read_safe(uint8_t len, volatile uint8_t *buff);
#endif

#endif
