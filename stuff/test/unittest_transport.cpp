/**
 * @file unittest_transport.c aka usart.c
 * 
 * USART basic library for ATMega328p MCU.
 * 
 * Defines several useful functions for use with USART0
 * standard peripheral module.
 */

#include "unittest_transport.h"

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#if defined(MCU_SIGNS) || defined(MCU_VERIFIES)
#include <util/crc16.h>
#endif

#include "usart.h"

static volatile uint8_t rx[USART_BUFF_LEN];
static volatile uint8_t tx[USART_BUFF_LEN];
static volatile uint8_t rx_in, tx_in; // the number of bytes in buffer
static volatile uint8_t rx_a, tx_a; // indices for state machine use
static volatile uint8_t rx_u, tx_u; // indices for user (the program) use
static volatile uint8_t usart_err;
static volatile uint8_t null; // dummy variable to dump inbox w/o reading
#ifdef MCU_SIGNS
static volatile uint8_t crc = CRC8_SEED;
static volatile uint8_t crc_enqueued = false;
#endif

static int8_t usart_write_blocking_unsafe(uint8_t data);
static inline uint8_t failed(int8_t code);

/**
 * Inits the USART0 module
 */
void usart_init(void)
{
	uint16_t ubrr = F_CPU / 8 / USART_BAUD - 1; // double speed
	sei();
	UBRR0H = (uint8_t)(ubrr >> 8);
	UBRR0L = (uint8_t)ubrr;
	UCSR0A = _BV(U2X0); // enable double speed
	// rec interrupts, rx & tx:
	UCSR0B = _BV(RXCIE0) | _BV(RXEN0) | _BV(TXEN0);
	// 8-bit frame, odd parity, 2 stop-bits:
	UCSR0C = _BV(UCSZ01) | _BV(UCSZ00); // | _BV(UPM01) | _BV(UPM00) |
		// _BV(USBS0);
}

/**
 * Switches off the USART RX interrupts
 */
void usart_rx_cli(void)
{
	UCSR0B &= ~_BV(RXCIE0);
}

/**
 * Switches on the USART RX interrupts
 */
void usart_rx_sei(void)
{
	UCSR0B |= _BV(RXCIE0);
}

/**
 * Puts a byte to the Write buffer.
 * 
 * @param data The byte to put to Write buffer.
 */
int8_t usart_enqueue(uint8_t data)
{
	if (tx_in && (tx_u == tx_a)) {
		return USART_ERR_WRITEFULL;
	}

	tx[tx_u] = data;
	RINC(tx_u);
	++tx_in;

#ifdef MCU_SIGNS
	crc = _crc8_ccitt_update(crc, data);
	crc_enqueued = false;
#endif

	return USART_SUCCESS;
}

/**
 * Initiates the process of sending the Write buffer.
 */
int8_t usart_send(void)
{
	if (!tx_in) {
		return USART_SUCCESS; // Don't even need to bother
	}

#ifdef MCU_SIGNS
	if (!crc_enqueued) {
		usart_enqueue_crc();
	}
#endif

	if (!(UCSR0B & _BV(UDRIE0))) { // if no Data Register Empty int.
		if (UCSR0A & _BV(UDRE0)) { // if Transmit buffer is ready
			UDR0 = tx[tx_a];
			RINC(tx_a);
			if (--tx_in) {
				UCSR0B |= _BV(UDRIE0); // turn on the above int.
			}
		} else {
			UCSR0B |= _BV(UDRIE0);
		}
	}
	return 0;
}

/**
 * Blocks until the Write buffer is empty.
 */
int8_t usart_flush_tx()
{
	volatile uint32_t i = 0;

	usart_send();

	while (UCSR0B & _BV(UDRIE0) && i++ < USART_SEND_MAXATTS)
		; // wait until the sending operation is complete

	if (USART_SEND_MAXATTS <= i) {
		return USART_ERR_MAXATTEMPTS;
	}
	return USART_SUCCESS;
}

#ifdef MCU_SIGNS
/**
 * Appends CRC8 of the latest chunk of data to the Write buffer.
 * 
 * Tries to enqueue the current value of CRC, 
 * resets its value and raises `crc_enqueued` flag
 * on success, reports on failure.
 */
int8_t usart_enqueue_crc()
{
	int8_t res;
	if (!failed(usart_enqueue(crc))) {
		crc = CRC8_SEED;
		crc_enqueued = true;
	}

	return res;
}
#endif

/**
 * Send a byte to USART in blocking way
 * 
 * @param data The byte to send
 */
int8_t usart_write_blocking(uint8_t data)
{
	return usart_write_blocking_unsafe(data);
}

/**
 * Checks whether the read buffer is NOT empty.
 */
uint8_t usart_available()
{
	return 0 != rx_in;
}

/**
 * Reads a byte from Read buffer w/o any signs.
 * 
 * Returns the value of the oldest byte in the Read buffer,
 * updates the Read buffer state and eliminates
 * `USART_ERR_READFULL` if such persists by the moment
 * of reading. Does not in any case perform any
 * control checks.
 */
uint8_t usart_read_unsafe()
{
	uint8_t data = rx[rx_u];

	RINC(rx_u);
	--rx_in;

	if (USART_ERR_READFULL == usart_err) {
		usart_rx_sei();
		usart_err = USART_SUCCESS; // Eliminate the error if there was
	}

	return data;
}

#ifdef MCU_VERIFIES
int8_t usart_read_safe(uint8_t len, volatile uint8_t *buff)
{
	uint8_t i, crc_calc = CRC8_SEED, crc_got;
	int err = 0;

	for (i = 0; i < len; ++i) {
		if (failed(err = usart_read_blocking(buff + i))) {
			return err;
		}
		crc_calc = _crc8_ccitt_update(crc, buff[i]));
	}

	if (failed(err = usart_read_blocking(&crc_got)) ||
	    crc_calc != crc_got) {
		return USART_ERR_SIGN;
	}

	return 0;
}
#endif

/**
 * Reads a byte from the input buffer in a blocking way w/o signs.
 * 
 * Blocks until USART read buffer is not empty or USART_MAX_ATTEMPTS
 * number of trials failed. If there is anything to read,
 * copies the oldest entry in read buffer to the `*data_out`.
 * Does not in any case permorm any control checks.
 * 
 * @param data_out Pointer to the variable to write to.
 */
int8_t usart_read_blocking(volatile uint8_t *data_out)
{
	volatile uint32_t i;

	for (i = 0; !usart_available() && i < USART_MAX_ATTEMPTS; ++i)
		;

	if (USART_MAX_ATTEMPTS <= i) {
		return USART_ERR_MAXATTEMPTS;
	} else {
		*data_out = usart_read_unsafe();

		return USART_SUCCESS;
	}
} // usart_read_blocking

/**
 * A brief check for status code being nonnegative
 * 
 * @param code The status code to check
 */
static inline uint8_t failed(int8_t code)
{
	return (code < 0);
}

/**
 * Sends a byte to USART in blocking way w/o any signs.
 * 
 * Writes the data to the USART Data Register and
 * blocks until the transmission is complete
 * or USART_MAX_ATTEMPTS number of trials has been achieved
 * 
 * @param data The byte being sent
 */
static int8_t usart_write_blocking_unsafe(uint8_t data)
{
	volatile uint32_t i;

	for (i = 0; !(UCSR0A & _BV(UDRE0)) && i < USART_MAX_ATTEMPTS; ++i)
		;

	if (USART_MAX_ATTEMPTS <= i) {
		return USART_ERR_MAXATTEMPTS;
	} else {
		UDR0 = data;
		return USART_SUCCESS;
	}
}

ISR(USART_RX_vect)
{
	usart_rx_cli();

	sei(); // we must not miss the timer event

	if ((UCSR0A & _BV(UPE0)) || (UCSR0A & _BV(DOR0))) {
		SKIP_CHAR(); // skip if parity error or data overrun
	} else {
		if ((USART_ERR_READFULL == usart_err) ||
		    (rx_in && (rx_a == rx_u))) {
			usart_err = USART_ERR_READFULL;

			return; // RX interrupts stays off
		} else {
			rx[rx_a] = UDR0;
			RINC(rx_a);
			++rx_in;
		}
	}

	usart_rx_sei();
}

ISR(USART_UDRE_vect)
{
	UDR0 = tx[tx_a];
	RINC(tx_a);
	if (!(--tx_in)) {
		UCSR0B &= ~_BV(UDRIE0); // turn off Data Register Empty int
#ifdef MCU_SIGNS
		crc = CRC8_SEED;
		crc_enqueued = false;
#endif
	}
}

EMPTY_INTERRUPT(USART_TX_vect);

void unittest_uart_begin()
{
	usart_init();
}
void unittest_uart_putchar(char c)
{
	usart_enqueue((uint8_t)c);
}
void unittest_uart_flush()
{
	usart_flush_tx();
}
void unittest_uart_end()
{
	cli();
}
